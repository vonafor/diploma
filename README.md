# Установка #

```
#!bash

pip install -r requirements.txt

```

[Установка Redis](https://www.digitalocean.com/community/tutorials/how-to-install-and-use-redis)

### Запуск брокера ###

```
#!bash

sudo service redis_6379 start

```


### Запуск воркера ###

```
#!bash

celery -A app.celery worker

```