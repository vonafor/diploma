import os


base_dir = os.path.abspath(os.path.dirname(__file__))


WTF_CSRF_ENABLED = True
APP_NAME = 'Majorum'
TEMPLATES_AUTO_RELOAD = True

# db
SQLALCHEMY_DATABASE_URI = 'postgresql:///majorum'
SQLALCHEMY_MIGRATE_REPO = os.path.join(base_dir, 'db_repository')
SQLALCHEMY_TRACK_MODIFICATIONS = False

# user
USER_ENABLE_CHANGE_USERNAME = False
USER_AFTER_LOGIN_ENDPOINT = 'user.profile'
USER_AFTER_CONFIRM_ENDPOINT = 'user.profile'
USER_UNAUTHORIZED_ENDPOINT = 'index'
USER_APP_NAME = APP_NAME

# email server
MAIL_SERVER = 'smtp.yandex.ru'
MAIL_PORT = 465
MAIL_USE_TLS = False
MAIL_USE_SSL = True

# babel
BABEL_DEFAULT_LOCALE = 'en'

# whoosh
WHOOSHEE_DIR = os.path.join(base_dir, 'whooshee')

# celery
CELERY_BROKER_URL = 'redis://localhost:6379/0'
CELERY_RESULT_BACKEND = 'redis://localhost:6379/0'
