from datetime import datetime
from flask_script import Manager
from flask_migrate import Migrate, MigrateCommand
from flask_user.passwords import hash_password

from app import app, db
from app.classroom.models import Classroom
from app.auth.models import User
from app.practice.models import Practice, Task
migrate = Migrate(app, db)

manager = Manager(app)
manager.add_command('db', MigrateCommand)


@manager.option('-t', '--table', help='For table')
def load_fixtures(table):
    if not table:
        return
    names = {
        'classroom': Classroom,
        'user': User,
        'practice': Practice,
        'task': Task,
    }
    now = datetime.now().date()
    all_data = {
        'classroom': [
            {'title': '09-308', 'id': 100},
            {'title': '09-309', 'id': 101},
            {'title': '09-310', 'id': 102},
            {'title': '09-311', 'id': 103},
            {'title': '09-312', 'id': 104},
            {'title': '09-313', 'id': 105},
        ],
        'user': [
            {'first_name': 'Игорь', 'last_name': 'Филиппов', 'classroom_id': 100, 'id': 96},
            {'first_name': 'Владилен', 'last_name': 'Лаврентьев', 'classroom_id': 100, 'id': 97},
            {'first_name': 'Осип', 'last_name': 'Логунов', 'classroom_id': 100, 'id': 98},
            {'first_name': 'Семён', 'last_name': 'Гаскаров', 'classroom_id': 100, 'id': 99},
            {'first_name': 'Витольд', 'last_name': 'Зимин', 'classroom_id': 100, 'id': 100},
            {'first_name': 'Василий', 'last_name': 'Абрамов', 'classroom_id': 100, 'id': 101},
            {'first_name': 'Артур', 'last_name': 'Вишняков', 'classroom_id': 100, 'id': 102},
            {'first_name': 'Денис', 'last_name': 'Родионов', 'classroom_id': 102, 'id': 103},
            {'first_name': 'Александр', 'last_name': 'Красильников', 'classroom_id': 103, 'id': 104},
            {'first_name': 'Кирилл', 'last_name': 'Денисов', 'classroom_id': 104, 'id': 105},
            {'first_name': 'Ярослав', 'last_name': 'Федотов', 'classroom_id': 104, 'id': 106, 'role': 2},
            {'first_name': 'Прохор', 'last_name': 'Одинцов', 'classroom_id': 104, 'id': 107, 'role': 2},
            {'first_name': 'Админ', 'last_name': 'Админский', 'id': 108, 'role': 3, 'username': 'admin'},
        ],
        'practice': [
            {'user_id': 106, 'title': 'Контрольная работа №1', 'id': 100},
            {'user_id': 106, 'title': 'Контрольная работа №2', 'id': 101},
            {'user_id': 106, 'title': 'Контрольная работа №3', 'id': 102},
        ],
        'task': [
            {'practice_id': 100, 'question': '''В стакане находятся бактерии. Через секунду каждая из бактерий делится пополам, затем каждая из получившихся бактерий через секунду делится пополам и так далее. Через минуту стакан полон. Через сколько секунд стакан был заполнен наполовину?''', 'answer': '59'},
            {'practice_id': 100, 'question': '''В доме 6 этажей. Во сколько раз путь по лестнице на шестой этаж длиннее, чем путь по той же лестнице на третий этаж, если пролёты между этажами имеют по одинаковому числу ступенек''', 'answer': '2.5'},
            {'practice_id': 100, 'question': '''В 6 часов часы пробили 6 ударов. Время, прошедшее от первого удара до шестого, равнялось 30 секундам. Если для того, чтобы пробить 6 раз, часам понадобилось 30 секунд, то сколько времени будет продолжаться бой часов в полдень или полночь, когда часы бьют 12 раз?''', 'answer': '66'},
            {'practice_id': 100, 'question': '''Хозяин сидит на берегу пруда, зарастающего сорняками. Каждый день число сорняков удваивается. Он собирается приступить к расчистке, как только зарастёт половина пруда. Через месяц половина пруда оказалась заросшей. Сколько дней у него остаётся на расчистку?''', 'answer': '1'},
            {'practice_id': 100, 'question': '''Когда моему отцу был 31 год, мне было 8 лет, а теперь отец старше меня вдвое. Сколько лет мне теперь?''', 'answer': '23'},
            {'practice_id': 100, 'question': '''Уезжая в командировку на 9 дней, инженер Додырин взял с собой кусок мыла прямоугольной формы. За неделю командировки кусок по всем направлениям уменьшился вдвое. Хватит ли остатка на последние 2 дня?''', 'answer': 'Нет'},
            {'practice_id': 101, 'question': '''Два пассажирских поезда, оба длиной по 250 м, идут навстречу друг другу с одинаковой скоростью 45 км/час. Сколько секунд пройдёт после того, как встретились машинисты, до того, как встретятся кондукторы последних вагонов?''', 'answer': '20'},
            {'practice_id': 101, 'question': '''На книжной полке в правильном порядке стоит трёхтомное собрание сочинений некоего автора. Толщина первого тома равна 17 мм, второго 15 мм, а третьего — 12 мм. Толщина переплёта равна 1 мм (она входит в толщину тома). Книжный червь прополз от первой страницы первого тома до последней страницы третьего тома. Какой длины путь прополз червяк?''', 'answer': '17'},
            {'practice_id': 101, 'question': '''Как быстро посчитать сумму всех чисел от 1 до 100?''', 'answer': '5050'},
            {'practice_id': 101, 'question': '''В соревнованиях по кубковой системе (с выбыванием проигравшего) принимают участие 100 команд. Сколько надо провести игр для выявления победителя?''', 'answer': '99'},
            {'practice_id': 102, 'question': '''Летела стая гусей: один гусь впереди, а два позади; один позади и два впереди; один между двумя и три в ряд. Сколько всего было гусей?''', 'answer': '3'},
            {'practice_id': 102, 'question': '''В комнате 4 угла. В каждом углу сидит кошка. Напротив каждой кошки сидит по три кошки. Сколько всего кошек в комнате?''', 'answer': '4'},
            {'practice_id': 102, 'question': '''На потолке комнаты сидели три мухи. Вспугнутые хозяйкой они все одновременно полетели. Какова вероятность, что в какой-то момент времени они вновь окажутся в одной плоскости?''', 'answer': '1'},
            {'practice_id': 102, 'question': '''Сумма углов в треугольнике?''', 'answer': '180'},
            {'practice_id': 102, 'question': '''Сколько будет 7 * 8?''', 'answer': '56'},
        ]
    }
    table_cls = names[table]
    data = all_data[table]
    user_manager = app.user_manager
    for i, row in enumerate(data):
        if table == 'user':
            username = 'student{}'
            if row.get('role') == 2:
                username = 'teacher{}'
            username = username.format(row['id'])

            if row.get('role') == 3:
                username = row['username']

            email = 'fake-mail{}@example.com'.format(i)
            row.update({
                'username': username,
                'is_enabled': True,
                'confirmed_at': now,
                'email': email,
                'university': 'КФУ',
                'password': hash_password(user_manager, '1a2345A')
            })
        if table_cls == 'classroom':
            row['graduated'] = now

        db.session.add(table_cls(**row))
    db.session.commit()


if __name__ == '__main__':
    manager.run()
