from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_mail import Mail
from flask_babel import Babel
from flaskext.markdown import Markdown
from flask_whooshee import Whooshee
from flask_admin import Admin
from app.make_celery import make_celery


app = Flask(__name__)
app.config.from_object('config')
app.config.from_object('private_config')
db = SQLAlchemy(app)
mail = Mail(app)
babel = Babel(app)
markdown = Markdown(app)
whooshee = Whooshee(app)
celery = make_celery(app)


from app.model_views import AppAdminIndexView
class AppAdmin(Admin):
    def add_menu_item(self, menu_item, target_category=None):
        if menu_item.name == 'Home':
            return
        super().add_menu_item(menu_item, target_category=target_category)

admin = AppAdmin(app, name=app.config['APP_NAME'], index_view=AppAdminIndexView(), template_mode='bootstrap3', 
    base_template='base_admin.html')


import app.classroom.views as fake
from app.auth import user_manager
from app import views
from app import filters
from app.practice.views import mod_practice
from app.material.views import mod_material


app.register_blueprint(mod_practice)
app.register_blueprint(mod_material)
