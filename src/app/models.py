from app import db


class BaseModelMixin(object):
    id = db.Column(db.Integer, primary_key=True)

    @classmethod
    def to_dict(cls, row):
        result = {}
        for column in cls.__table__.columns:
            result[column.name] = getattr(row, column.name)
        return result
