from datetime import datetime
from flask import render_template, url_for
from flask_user import current_user
from app import app
from app.auth.enums import Role


def get_user_menu():
    if not getattr(current_user, 'is_authenticated', False):
        return []
    menu = [('Профиль', url_for('user.profile')), 
        ('Материалы', url_for('material.list_view'))]
    if current_user.has_role(Role.ADMIN):
        menu.append(('Группы', url_for('classroom.index_view')))
        menu.append(('Пользователи', url_for('user.index_view')))
        return menu
    menu.append(('Контрольные работы', url_for('practice.homework_list_view')))
    if current_user.has_role(Role.TEACHER):
        menu.append(('База заданий', url_for('practice.list_view')))
    return menu


@app.context_processor
def inject_app_name():
    return {
        'app_name': app.config['APP_NAME'],
        'user_menu': get_user_menu(),
        'Role': Role,
        'current_time': datetime.now()
    }


@app.route('/')
def index():
    return render_template('index.html')
