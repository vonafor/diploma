from collections import defaultdict
import json
from flask import Blueprint, render_template, redirect, url_for, flash
from flask_user import current_user, roles_accepted
from sqlalchemy.orm import aliased
from sqlalchemy import and_
from .models import Practice, Task, Homework, WorkResult, TaskResult
from .forms import PracticeCreateForm, TaskCreateForm, HomeworkAddForm, get_dynamic_form
from .tasks import homework_notify
from .enums import TaskTypeEnum
from app import db
from app.auth.enums import Role
from app.auth.decorators import cake
from app.auth.models import User
from app.utils import get_one_or_create

mod_practice = Blueprint('practice', __name__, url_prefix='/practice',
                         template_folder='templates')


@mod_practice.route('/', methods=['GET', 'POST'])
@cake(Role.TEACHER)
def list_view():

    practices = Practice.query.filter_by(user_id=current_user.id)

    return render_template('practice/list.html', practices=practices)


@mod_practice.route('/add', methods=['GET', 'POST'])
@cake(Role.TEACHER)
def create_view():
    form = PracticeCreateForm()
    if form.validate_on_submit():
        practice = Practice(title=form.title.data, user_id=current_user.id)
        db.session.add(practice)
        db.session.commit()
        return redirect(url_for('practice.edit_view', id=practice.id))
    return render_template('practice/create.html', form=form)


@mod_practice.route('/edit/<int:id>', methods=['GET', 'POST'])
@cake(Role.TEACHER)
def edit_view(id):
    practice = Practice.query.get_or_404(id)
    form = PracticeCreateForm(obj=practice)
    if form.validate_on_submit():
        practice.title = form.title.data
        db.session.commit()
        return redirect(url_for('practice.edit_view', id=practice.id))
    return render_template('practice/create.html', form=form, practice=practice)

@mod_practice.route('/delete/<int:id>', methods=['GET', 'POST'])
@cake(Role.TEACHER)
def delete_view(id):
    practice = Practice.query.get_or_404(id)
    if practice.user_id == current_user.id:
        db.session.delete(practice)
        db.session.commit()
    return redirect(url_for('practice.list_view', id=id))


@mod_practice.route('/<int:practice_id>/add_task', methods=['GET', 'POST'])
@cake(Role.TEACHER)
def add_task(practice_id):
    form = TaskCreateForm()
    practice = Practice.query.get_or_404(practice_id)
    if form.validate_on_submit():
        task = Task(question=form.question.data,
                    type=form.type.data,
                    values=form.values.data,
                    answer=form.answer.data,
                    practice_id=practice_id)
        db.session.add(task)
        db.session.commit()
        return redirect(url_for('practice.edit_view', id=practice_id))
    return render_template('practice/task_create.html', form=form, practice=practice)


@mod_practice.route('/edit_task/<int:id>', methods=['GET', 'POST'])
@cake(Role.TEACHER)
def edit_task(id):
    task = Task.query.get_or_404(id)
    form = TaskCreateForm(obj=task)
    if form.validate_on_submit():
        task.question = form.question.data
        task.type = form.type.data
        task.values = form.values.data
        task.answer = form.answer.data
        db.session.commit()
        return redirect(url_for('practice.edit_view', id=task.practice_id))
    return render_template('practice/task_create.html', form=form, practice=task.practice)


@mod_practice.route('/delete_task/<int:id>', methods=['GET', 'POST'])
@cake(Role.TEACHER)
def delete_task(id):
    task = Task.query.get_or_404(id)
    if task.practice.user_id == current_user.id:
        db.session.delete(task)
        db.session.commit()
    return redirect(url_for('practice.list_view'))


@mod_practice.route('/homework/', methods=['GET'])
@cake(Role.TEACHER, Role.STUDENT)
def homework_list_view():
    query = Homework.query.join(Homework.practice).order_by(Homework.finish_date.desc())
    if current_user.role == Role.TEACHER:
        query = query.filter_by(user_id=current_user.id).add_entity(Practice)
    else:
        result_table = aliased(WorkResult)
        query = query.filter(Homework.classroom_id==current_user.classroom_id).join(User).outerjoin(
            result_table, and_(result_table.homework_id==Homework.id, result_table.user_id==current_user.id)
        ).add_entity(result_table).add_entity(Practice).add_entity(User)
    return render_template('practice/homework_list.html', query=query)


@mod_practice.route('/homework/add/', methods=['GET', 'POST'])
@cake(Role.TEACHER)
def homework_add_view():
    form = HomeworkAddForm()
    if form.validate_on_submit():
        homeworks = []
        for classroom in form.classrooms.data:
            homework, created = get_one_or_create(
                db.session, 
                Homework,
                practice_id=form.practice.data.id,
                classroom_id=classroom.id,
                defaults={
                    'finish_date': form.finish_date.data
                }
            )
            if not created:
                homework.finish_date = form.finish_date.data
            db.session.add(homework)
            if created:
                homeworks.append(homework)

        db.session.commit()

        if homeworks:

            homeworks = [h.id for h in homeworks]

            homework_notify.delay(homeworks)
            flash('Оповещения о домашней работе были отправлены на почту!', 'success')
        else:
            flash('Контрольная работа уже добавлена', 'error')
        return redirect(url_for('practice.homework_list_view'))
    return render_template('practice/homework_add.html', form=form)


@mod_practice.route('/homework/delete/<int:id>')
@cake(Role.TEACHER)
def homework_delete_view(id):
    homework = Homework.query.get_or_404(id)
    practice = homework.practice
    if practice.user_id == current_user.id:
        db.session.delete(homework)
        db.session.commit()
    return redirect(url_for('practice.homework_list_view'))


def _work_form(homework):
    practice_id = homework.practice_id
    tasks = Task.query.filter_by(practice_id=practice_id)
    return form_cls(), questions

@mod_practice.route('/homework/solve/<int:id>', methods=['GET'])
@cake(Role.STUDENT)
def homework_solve(id):
    work_result = WorkResult.query.filter_by(homework_id=id, user_id=current_user.id).first()
    if work_result and not work_result.homework.is_active:
        return redirect(url_for('practice.homework_list_view'))
    homework = Homework.query.get_or_404(id)
    practice_id = homework.practice_id
    tasks = Task.query.filter_by(practice_id=practice_id)
    form_cls, questions = get_dynamic_form(tasks, current_user.id)
    data = work_result.generate_data() if work_result else {}
    return render_template('practice/homework_solve.html', form=form_cls(data=data), homework=homework, 
        questions=questions)


@mod_practice.route('/homework/answer/<int:id>', methods=['POST'])
@cake(Role.STUDENT)
def homework_answer(id):
    homework = Homework.query.get_or_404(id)
    practice_id = homework.practice_id
    tasks = Task.query.filter_by(practice_id=practice_id)
    form = get_dynamic_form(tasks, current_user.id)[0]()

    work_result, created = get_one_or_create(
        db.session,
        WorkResult,
        homework_id=homework.id,
        user_id=current_user.id
    )

    if not created and not homework.is_active:
        return redirect(url_for('practice.homework_list_view'))

    if not form.validate_on_submit():
        return redirect(url_for('practice.homework_solve', id=id))

    user_answers = defaultdict(set)
    for field in form:
        if field.name in ['csrf_token', 'submit']:
            continue

        ids = field.name.split('_')[1:]
        id = int(ids[0])
        if len(ids) == 1:
            user_answers[id] = field.data
        else:
            value = int(ids[1])
            if field.data:
                user_answers[id].add(value)

    total = 0
    correct = 0
    result_tasks = []
    for task in tasks:
        total += 1
        if task.type == TaskTypeEnum.RAW_VALUE.value:
            right_answer = task.answer
        else:
            right_answer = set(json.loads(task.answer))

        user_answer = user_answers[task.id]

        result_tasks.append(TaskResult(
            work_result_id=work_result.id,
            user_input=json.dumps(list(user_answer)) if isinstance(user_answer, set) else user_answer,
            task_id=task.id,
            task_type=task.type
        ))

        if user_answer == right_answer:
            correct += 1

    work_result.total = total
    work_result.correct = correct

    db.session.add(work_result)
    work_result.task_results.delete()
    work_result.task_results.extend(result_tasks)
    db.session.commit()

    flash('Задание отправлено', 'success')
    return redirect(url_for('practice.homework_list_view'))


@mod_practice.route('/homework/show_result/<int:id>')
@cake(Role.TEACHER)
def show_result(id):
    homework = Homework.query.get_or_404(id)
    query = User.query.filter_by(
        classroom_id=homework.classroom_id
    ).outerjoin(
        WorkResult,
        and_(WorkResult.user_id == User.id, WorkResult.homework_id == homework.id)
    ).add_entity(
        WorkResult
    ).order_by(User.last_name, User.first_name)
    return render_template('practice/result.html', homework=homework,
        query=query)
