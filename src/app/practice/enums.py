import enum

from app.enums import EnumMixin


class TaskTypeEnum(EnumMixin, enum.Enum):
    RAW_VALUE = 1
    SINGLE_VALUE = 2

    __labels__ = {
        RAW_VALUE: 'Ввод',
        SINGLE_VALUE: 'Выбор'
    }


class WorkStatus:
    STARTED = 1
    FINISHED = 2
