from datetime import datetime
import json
from sqlalchemy.schema import UniqueConstraint
from app import db
from app.models import BaseModelMixin
from .enums import TaskTypeEnum, WorkStatus


class Practice(BaseModelMixin, db.Model):
    title = db.Column(db.String(250), nullable=False, default='')
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    user = db.relationship('User', foreign_keys=user_id)
    tasks = db.relationship('Task', backref='practice', lazy='dynamic', cascade='all, delete-orphan')

    def __repr__(self):
        return self.title


class Task(BaseModelMixin, db.Model):
    practice_id = db.Column(db.Integer, db.ForeignKey('practice.id'), nullable=False)
    question = db.Column(db.String(400), nullable=False, default='')
    type = db.Column(db.Integer, nullable=False, default=TaskTypeEnum.RAW_VALUE.value)
    values = db.Column(db.String, server_default='', nullable=False)  # все ответы
    answer = db.Column(db.String, server_default='', nullable=False)  # правильные


class Homework(BaseModelMixin, db.Model):
    practice_id = db.Column(db.Integer, db.ForeignKey('practice.id'))
    practice = db.relationship('Practice', foreign_keys=practice_id)
    max_questions = db.Column(db.Integer)
    finish_date = db.Column(db.DateTime, nullable=False)
    classroom_id = db.Column(db.Integer, db.ForeignKey('classroom.id'))
    classroom = db.relationship('Classroom', foreign_keys=classroom_id)
    results = db.relationship('WorkResult', lazy='dynamic', cascade='all, delete-orphan')

    __table_args__ = (
        UniqueConstraint('practice_id', 'classroom_id', name='_practice_classroom_unique'),
    )

    @property
    def is_active(self):
        return self.finish_date >= datetime.now()


class WorkResult(BaseModelMixin, db.Model):
    homework_id = db.Column(db.Integer, db.ForeignKey('homework.id'))
    homework = db.relationship('Homework', foreign_keys=homework_id)
    total = db.Column(db.Integer, default=0)
    correct = db.Column(db.Integer, default=0)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    user = db.relationship('User', foreign_keys=user_id)
    status = db.Column(db.Integer, nullable=False, server_default=str(WorkStatus.STARTED))
    task_results = db.relationship('TaskResult', lazy='dynamic', cascade='all, delete-orphan')

    __table_args__ = (
        UniqueConstraint('homework_id', 'user_id', name='_homework_user_unique'),
    )

    def generate_data(self):
        data = {}
        for result in self.task_results:
            task_id = result.task_id
            value = result.user_input
            if result.task_type == TaskTypeEnum.RAW_VALUE.value:
                data['answer_{}'.format(result.task_id)] = value
            else:
                values = json.loads(value)
                for val in values:
                    data['answer_{}_{}'.format(task_id, val)] = True
        return data


class TaskResult(BaseModelMixin, db.Model):
    work_result_id = db.Column(db.Integer, db.ForeignKey('work_result.id'), nullable=False)
    work_result = db.relationship('WorkResult', foreign_keys=work_result_id)
    user_input = db.Column(db.String, default='')
    task_id = db.Column(db.Integer, nullable=False)
    task_type = db.Column(db.Integer, nullable=False)
