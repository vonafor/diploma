from itertools import groupby
from operator import itemgetter
from flask_mail import Message
from app import celery, mail

from flask_user.emails import _render_email as render_email

from app.auth.models import User
from .models import Homework


@celery.task()
def homework_notify(homeworks):
    query = User.query.with_entities(
        User.email,
        Homework.id,
        Homework.finish_date,
    ).join(
        Homework, 
        User.classroom_id==Homework.classroom_id
    ).filter(
        Homework.id.in_(homeworks)
    ).order_by(Homework.id).distinct()

    for (homework_id, finish_date), q in groupby(query, key=itemgetter(1, 2)):
        emails = [i[0] for i in q]
        subject, html_message, text_message = render_email(
            'emails/homework_notify',
            homework_id=homework_id,
            finish_date=finish_date
        )
        message = Message(
            subject,
            recipients=emails,
            html=html_message,
            body=text_message
        )
        mail.send(message)
