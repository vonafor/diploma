import json
from hashlib import md5
from flask_wtf import FlaskForm

from wtforms import (
    StringField, SelectField, TextAreaField, SubmitField, DateTimeField, BooleanField)
from wtforms.ext.sqlalchemy.fields import QuerySelectField, QuerySelectMultipleField
from wtforms.validators import DataRequired, Length
from flask_user import current_user

from app.classroom.models import Classroom

from .enums import TaskTypeEnum
from .models import Practice


class PracticeCreateForm(FlaskForm):
    title = StringField('Название', validators=[DataRequired(), Length(max=250)])
    submit = SubmitField('Сохранить')


class TaskCreateForm(FlaskForm):
    question = TextAreaField('Вопрос', validators=[DataRequired(), Length(max=400)])
    type = SelectField('Тип', coerce=int, choices=TaskTypeEnum.choices(), validators=[DataRequired()])
    values = StringField('Ответы')
    answer = StringField('Правильный ответ', validators=[DataRequired()])
    newAnswer = StringField()
    submit = SubmitField('Сохранить')

    def validate(self):
        result = super().validate()
        if not result:
            return result
        if self.type.data == TaskTypeEnum.SINGLE_VALUE.value:
            values = json.loads(self.values.data)
            if len(values) < 2:
                self.newAnswer.errors.append('Должен быть выбор')
                return False
            answer = json.loads(self.answer.data)
            if not answer:
                self.newAnswer.errors.append('Должен быть правильный ответ')
                return False
        return True


def _practice_query():
    return Practice.query.filter_by(user_id=current_user.id)


class HomeworkAddForm(FlaskForm):
    finish_date = DateTimeField('Крайний срок', format='%d.%m.%Y %H:%M', 
        validators=[DataRequired()])
    practice = QuerySelectField('Задание', allow_blank=False, get_pk=lambda x: x.id,
        query_factory=_practice_query)
    classrooms = QuerySelectMultipleField('Группы', allow_blank=False, 
        query_factory=lambda: Classroom.query.all())
    submit = SubmitField('Добавить')


def get_dynamic_form(tasks, salt):
    class BaseForm(FlaskForm):
        pass

    questions = {}
    fields = []
    for task in tasks:
        order = int(md5('{}{}'.format(task.id, salt).encode('utf8')).hexdigest()[:3], 16)
        if task.type == TaskTypeEnum.RAW_VALUE.value:
            id = 'answer_{}'.format(task.id)
            questions[id] = task.question
            fields.append((order, (id, StringField, 'Ответ')))
        else:
            boxes = json.loads(task.values)
            local_fields = []
            for i, box in enumerate(boxes):
                label = box['value']
                id = 'answer_{}_{}'.format(task.id, box['id'])
                if i == 0:
                    questions[id] = task.question
                local_fields.append((id, BooleanField, label))
            fields.append((order, local_fields))

    for i, f in sorted(fields, key=lambda x: x[0]):
        if isinstance(f, list):
            for name, field_cls, label in f:
                setattr(BaseForm, name, field_cls(label))
        else:
            setattr(BaseForm, f[0], f[1](f[2]))

    BaseForm.submit = SubmitField('Сохранить')

    return BaseForm, questions