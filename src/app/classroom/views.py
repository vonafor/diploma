from flask import Blueprint
from app.model_views import AdminModelView
from app import db, admin
from .models import Classroom


class ClassroomAdminModelView(AdminModelView):
    column_labels = {
        'title': 'Номер',
        'graduated': 'Выпуск',
    }


admin.add_view(ClassroomAdminModelView(Classroom, db.session, name='Группы'))
