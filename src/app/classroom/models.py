from app import db
from app.models import BaseModelMixin


class MaterialGroup(BaseModelMixin, db.Model):
    material_id = db.Column(db.Integer, db.ForeignKey('material.id'), nullable=False)
    classroom_id = db.Column(db.Integer, db.ForeignKey('classroom.id'), nullable=False)


class Classroom(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(250), nullable=False, default='')
    graduated = db.Column(db.Date())
    materials = db.relationship('MaterialGroup', lazy='dynamic', cascade='all, delete-orphan')

    def __repr__(self):
        return self.title
