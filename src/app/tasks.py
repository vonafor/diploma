from flask_user.emails import send_email as base_send_email
from app import celery


@celery.task()
def send_email(*args, **kwargs):
    return base_send_email(*args, **kwargs)


async_send_email = send_email.delay
