class EnumMixin:

    @classmethod
    def choices(cls):
        return [(choice.value, cls.__labels__[choice.value]) for choice in cls]

    @classmethod
    def values(cls):
        return [choice.value for choice in cls]

    @classmethod
    def label(cls, value):
        return cls.__labels__[value]
