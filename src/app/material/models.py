from datetime import datetime
from app import db, whooshee
from app.models import BaseModelMixin
from app.classroom.models import MaterialGroup
from app.auth.enums import Role
from .enums import StatusEnum


@whooshee.register_model('title', 'article')
class Material(BaseModelMixin, db.Model):
    __searchable__ = ['title', 'article']
    title = db.Column(db.String(250), nullable=False, default='')
    author_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    author = db.relationship('User', foreign_keys=author_id)
    status = db.Column(db.Integer, default=StatusEnum.DRAFT.value, nullable=False)
    article = db.Column(db.String, nullable=False)
    published = db.Column(db.DateTime, nullable=False)
    commets = db.relationship('Comment', lazy='dynamic', cascade='all, delete-orphan')
    groups = db.relationship('MaterialGroup', lazy='dynamic', cascade='all, delete-orphan')

    @property
    def status_label(self):
        return StatusEnum.label(self.status)

    def update_groups(self, groups):
        self.groups.delete()
        self.groups.extend(MaterialGroup(
            material_id=self.id, classroom_id=group.id) for group in groups)


class Comment(BaseModelMixin, db.Model):
    author_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    author = db.relationship('User', foreign_keys=author_id)
    published = db.Column(db.DateTime, nullable=False, default=datetime.now)
    material_id = db.Column(db.Integer, db.ForeignKey('material.id'), nullable=False)
    material = db.relationship('Material', foreign_keys=material_id)
    message = db.Column(db.String(250), nullable=False, server_default='')

    def allow_edit(self, user):
        return self.author_id == user.id or user.has_role(Role.ADMIN)