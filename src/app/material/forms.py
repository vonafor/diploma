from flask_wtf import FlaskForm
from wtforms import (
    StringField, SelectField, TextAreaField, SubmitField, DateTimeField)
from wtforms.ext.sqlalchemy.fields import QuerySelectMultipleField
from wtforms.validators import DataRequired, Length

from app.classroom.models import Classroom
from .enums import StatusEnum


class MaterialCreateForm(FlaskForm):
    title = StringField('Название', validators=[DataRequired(), Length(max=250)])
    status = SelectField('Статус', coerce=int, choices=StatusEnum.choices(), validators=[DataRequired()])
    published = DateTimeField('Дата публикации', format='%d.%m.%Y %H:%M', validators=[DataRequired()])
    article = TextAreaField(validators=[DataRequired()])
    groups = QuerySelectMultipleField('Группы', allow_blank=False, 
        query_factory=lambda: Classroom.query.all())
    submit = SubmitField('Сохранить')


class CommentForm(FlaskForm):
    message = TextAreaField('Комментарий:', validators=[DataRequired(), Length(max=250)])
    submit = SubmitField('Отправить')
