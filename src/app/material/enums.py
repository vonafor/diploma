import enum

from app.enums import EnumMixin


class StatusEnum(EnumMixin, enum.Enum):
    DRAFT = 1
    PUBLISHED = 2

    __labels__ = {
        DRAFT: 'Черновик',
        PUBLISHED: 'Опубликован'
    }
