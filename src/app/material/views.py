from datetime import datetime
from flask import Blueprint, render_template, redirect, url_for, request, jsonify
from flask_user import current_user, confirm_email_required
from flask_login import login_required

from app import db
from app.auth.decorators import cake
from app.auth.enums import Role
from app.classroom.models import MaterialGroup, Classroom

from .models import Material, Comment
from .forms import MaterialCreateForm, CommentForm
from .enums import StatusEnum


mod_material = Blueprint('material', __name__, url_prefix='/material',
                         template_folder='templates')


@mod_material.route('/', methods=['GET', 'POST'])
@mod_material.route('/<int:page>', methods=['GET', 'POST'])
@login_required
@confirm_email_required
def list_view(page=1):

    query = Material.query
    q = request.args.get('q')
    if q:
        query = query.whooshee_search(q)

    if current_user.role == Role.STUDENT:
        query = query.outerjoin(MaterialGroup).filter(
            (MaterialGroup.classroom_id==current_user.classroom_id) |
            (MaterialGroup.classroom_id==None)
        )

    query = query.filter(
        (Material.author_id == current_user.id) |
        ((Material.status == StatusEnum.PUBLISHED.value) &
         (Material.published <= datetime.now()))
    ).order_by(Material.published.desc()).distinct()

    materials = query.paginate(page, 5, True)

    return render_template('material/list.html', materials=materials)


@mod_material.route('/add', methods=['GET', 'POST'])
@cake(Role.TEACHER)
def create_view():
    form = MaterialCreateForm()
    if form.validate_on_submit():
        material = Material(
            title=form.title.data,
            status=form.status.data,
            published=form.published.data,
            article=form.article.data,
            author_id=current_user.id
        )
        db.session.add(material)
        material.update_groups(form.groups.data)
        db.session.commit()
        return redirect(url_for('material.list_view'))
    return render_template('material/create.html', form=form)


@mod_material.route('/delete/<int:id>')
@cake(Role.TEACHER)
def delete_view(id):
    material = Material.query.get_or_404(id)
    if current_user.id == material.author_id:
        db.session.delete(material)
        db.session.commit()
    return redirect(url_for('material.list_view'))



@mod_material.route('/edit/<int:id>', methods=['GET', 'POST'])
@cake(Role.TEACHER)
def edit_view(id):
    material = Material.query.get_or_404(id)
    if current_user.id != material.author_id:
        return redirect(url_for('material.list_view'))
    form = MaterialCreateForm(obj=material)
    if form.validate_on_submit():
        material.title = form.title.data
        material.status = form.status.data
        material.published = form.published.data
        material.article = form.article.data
        material.update_groups(form.groups.data)
        db.session.commit()
        return redirect(url_for('material.list_view'))
    form.groups.data = material.groups.join(Classroom).with_entities(Classroom)
    return render_template('material/create.html', form=form, material=material)


@mod_material.route('/view/<int:id>', methods=['GET'])
@login_required
@confirm_email_required
def item_view(id):
    material = Material.query.get_or_404(id)
    comments = Comment.query.filter_by(material_id=id)
    comment_form = CommentForm()
    return render_template('material/view.html', material=material,
                           comments=comments, comment_form=comment_form)


@mod_material.route('/comments/add/<int:material_id>', methods=['POST'])
@login_required
@confirm_email_required
def comment_add(material_id):
    form = CommentForm()
    if form.validate_on_submit():
        comment = Comment(
            author_id=current_user.id,
            material_id=material_id,
            message=form.message.data
        )
        db.session.add(comment)
        db.session.commit()
    return redirect(url_for('material.item_view', id=material_id))


@mod_material.route('/comments/delete/<int:id>')
@login_required
@confirm_email_required
def comment_delete(id):
    comment = Comment.query.get_or_404(id)
    material_id = comment.material_id
    if comment.allow_edit(current_user):
        db.session.delete(comment)
        db.session.commit()
    return redirect(url_for('material.item_view', id=material_id))


@mod_material.route('/comments/edit/', methods=['POST'])
@login_required
@confirm_email_required
def edit_comment():
    comment_id = request.form.get('id')
    new_value = request.form.get('value')
    if comment_id and new_value:
        id = int(comment_id.replace('comment_', ''))
        comment = Comment.query.get_or_404(id)
        if comment.allow_edit(current_user):
            comment.message = new_value[:250]
            db.session.commit()
        return new_value
    return redirect(url_for('material.list_view'))

