from flask_user import UserMixin, SQLAlchemyAdapter

from app import db
from app.auth.enums import SexEnum

from .enums import Role


class User(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)

    username = db.Column(db.String(50), nullable=False, unique=True)
    password = db.Column(db.String(255), nullable=False, default='')

    email = db.Column(db.String(255), nullable=False, unique=True)
    confirmed_at = db.Column(db.DateTime())
    is_enabled = db.Column(db.Boolean(), nullable=False, default=False)

    first_name = db.Column(db.String(50), nullable=False, default='')
    last_name = db.Column(db.String(50), nullable=False, default='')
    sex = db.Column(db.Integer, default=SexEnum.MALE.value)
    university = db.Column(db.String(250), nullable=False, default='', server_default='')
    bio = db.Column(db.String(800), nullable=False, default='', server_default='')
    classroom_id = db.Column(db.Integer, db.ForeignKey('classroom.id'))
    classroom = db.relationship('Classroom', foreign_keys=classroom_id)
    role = db.Column(db.Integer, server_default=str(Role.STUDENT))

    def has_role(self, *roles):
        return self.role in roles

    @property
    def first_and_last_name(self):
        return ' '.join([self.first_name, self.last_name])

    @property
    def fio(self):
        return ' '.join([self.last_name, self.first_name])
    


db_adapter = SQLAlchemyAdapter(db, User)
