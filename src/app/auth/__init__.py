from flask_user import UserManager
from app import app
from app.tasks import async_send_email
from .views import user_profile
from .models import db_adapter

user_manager = UserManager(
    db_adapter=db_adapter,
    app=app,
    user_profile_view_function=user_profile,
    send_email_function=async_send_email,
)
