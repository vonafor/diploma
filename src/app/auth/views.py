from flask import render_template, current_app, redirect, url_for, flash
from flask_login import login_required
from flask_user import confirm_email_required, current_user
from app.model_views import AdminModelView

from app import db, admin

from .forms import UserProfileChangeForm
from .models import User
from .enums import SexEnum


@login_required
@confirm_email_required
def user_profile():
    user_manager = current_app.user_manager
    db_adapter = user_manager.db_adapter

    form = UserProfileChangeForm()
    if form.validate_on_submit():
        db_adapter.update_object(
            current_user,
            first_name=form.first_name.data,
            last_name=form.last_name.data,
            sex=form.sex.data,
            bio=form.bio.data,
            university=form.university.data,
        )
        db_adapter.commit()
        flash("Изменения успешно сохранены", "success")
        return redirect(url_for('user.profile'))

    form.process(obj=current_user)
    return render_template(user_manager.user_profile_template,
                           form=form)


class UserAdminModelView(AdminModelView):
    column_exclude_list = ('password', 'bio')
    form_excluded_columns = ('password', 'confirmed_at')
    can_create = False
    can_delete = False

    column_labels = {
        'role': 'Роль',
        'bio': 'Обо мне',
        'university': 'Университет',
        'sex': 'Пол',
        'first_name': 'Имя',
        'last_name': 'Фамилия',
        'is_enabled': 'Активен',
        'confirmed_at': 'Дата подтверждения',
        'classroom': 'Группа'
    }

    column_choices = form_choices = {
        'role': [
            (1, 'Студент'),
            (2, 'Преподаватель'),
            (3, 'Админ')
        ],
        'sex': SexEnum.choices()
    }

    form_args = {
        'role': {
            'coerce': int
        },
        'sex': {
            'coerce': int
        }
    }


admin.add_view(UserAdminModelView(User, db.session, name='Пользователи'))
