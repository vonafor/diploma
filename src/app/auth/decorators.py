from functools import wraps
from flask_user import roles_accepted, confirm_email_required
from flask_login import login_required


def cake(*roles):
    def wrapper(f):
        @wraps(f)
        @login_required
        @confirm_email_required
        @roles_accepted(*roles)
        def wrapped(*args, **kwargs):
            return f(*args, **kwargs)
        return wrapped
    return wrapper
