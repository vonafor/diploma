from flask_wtf import FlaskForm

from wtforms import StringField, SelectField, TextAreaField, SubmitField
from wtforms.validators import DataRequired, Length

from app.auth.enums import SexEnum


class UserProfileChangeForm(FlaskForm):
    first_name = StringField('Имя', validators=[DataRequired(), Length(max=50)])
    last_name = StringField('Фамилия', validators=[DataRequired(), Length(max=50)])
    sex = SelectField('Пол', coerce=int, choices=SexEnum.choices(), validators=[DataRequired()])
    university = StringField('Университет', validators=[DataRequired(), Length(max=250)])
    bio = TextAreaField('Обо мне', validators=[Length(max=800)])

    submit = SubmitField('Сохранить')
