import enum

from app.enums import EnumMixin


class SexEnum(EnumMixin, enum.Enum):
    FEMALE = 1
    MALE = 2

    __labels__ = {
        FEMALE: 'Женский',
        MALE: 'Мужской'
    }


class Role:
    STUDENT = 1
    TEACHER = 2
    ADMIN = 3
