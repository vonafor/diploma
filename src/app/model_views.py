from datetime import date, datetime
from functools import partial
from flask import redirect, url_for, request
from flask_admin.contrib.sqla import ModelView
from flask_admin import AdminIndexView, expose
from flask_user import current_user
from .filters import datetimeformat


def datetime_formatter(format='%d.%m.%Y %H:%M'):
    return lambda view, value: datetimeformat(value, format=format)


class AdminModelView(ModelView):

    column_type_formatters = {
        datetime: datetime_formatter(),
        date: datetime_formatter(format='%d.%m.%Y')
    }

    def is_accessible(self):
        from app.auth.enums import Role
        return current_user.is_authenticated and current_user.has_role(Role.ADMIN)

    def inaccessible_callback(self, name, **kwargs):
        # redirect to login page if user doesn't have access
        return redirect(url_for('user.login', next=request.url))


class AppAdminIndexView(AdminIndexView):

    @expose('/')
    def index(self):
        if not current_user.is_authenticated:
            return redirect(url_for('user.login'))
        return super(AppAdminIndexView, self).index()

