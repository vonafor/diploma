$(function() {
    var form = $('form')[0];
    var typeField = $('form #type');
    var answerField = $('form #answer');
    var valuesField = $('form #values');
    var answersSet = $('form #answersSet');
    var valuesGroup = valuesField.parent();

    typeField.change(function(){
        render();
    });

    var CURRENT_TYPE = typeField[0].value;
    var values = [];
    if(valuesField.val()){
        values = $.parseJSON(valuesField.val());
    }
    var LAST_ID = 0;
    if(values.length){
        LAST_ID = _.max(values, function(d){
            return d.id;
        }).id + 1;
    }

    var template = _.template($('#valueFieldTemplate').html());


    function render(){
        var type = typeField[0].value;
        if(type == 1){
            answerField.parent().show();
            answersSet.hide();
            if(CURRENT_TYPE != 1){
                answerField.val('');
            }
            updateValues(null, null, true);
            $('form .surrogat').remove();
        } else {
            answerField.parent().hide();
            answersSet.show();
        }
        CURRENT_TYPE = type;
    }
    valuesField.parent().hide();
    render();
    if(values && CURRENT_TYPE != 1){
        _.each(values, function(el){
            var extra = '';
            var rights = $.parseJSON(answerField.val());
            if(_.contains(rights, el.id)){
                extra = 'checked';
            }
            insertNewField(el.id, el.value, extra);
        });
    }

    var ENTER_KEY = 13;
    $(document).on("keypress", ":input:not(textarea)", function(event) {
        if (event.keyCode == ENTER_KEY) {
            event.preventDefault();
        }
    });

    $('#newAnswer').on("keyup", function(e){
        var $input = $(e.target);
        var val = $input.val().trim();

        if (e.which !== ENTER_KEY || !val || values.indexOf(val) != -1) {
            return;
        }
        var extra = '';
        var index = LAST_ID;
        if(values.length == 0){
            extra = 'checked';
            answerField.val(JSON.stringify([index]));
        }

        insertNewField(index, val, extra);
        updateValues(index, val, false);
        $input.val('');
    });

    function updateValues(id, value, clear){
        clear = clear || false;
        if(clear){
            values = [];
        } else {
            values.push({id: id, value: value});
            LAST_ID = id + 1;
        }
        valuesField.val(JSON.stringify(values));
    }

    function insertNewField(id, title, extra){
        answersSet.append(template({title: title, extra: extra, id: id}));

        var optionField = $('#optionsRadios'+id);
        optionField.change(function(){
            answerField.val(getRightAnswer());
        });
        $('#inputField'+id).on('input', function(e){
            var data = _.findWhere(values, {id: id});
            data['value'] = this.value;
            refreshValuesField();
        });
        $('#destroyBtn'+id).on('click', function(e) {
            values = _.reject(values, function(d){
                return d.id == id;
            });
            this.closest('.surrogat').remove();
            refreshValuesField();
            refreshRightAnswerField();
        })
    }

    function getRightAnswer(){
        var type = typeField[0].value;
        if(type == 1){
            return answerField.val();
        } else {
            var result = [];
            $('form .surrogat :checked').each(function(){
                result.push(+$(this).attr('value'));
            });
            return JSON.stringify(result);
        }
    }

    function refreshRightAnswerField(){
        answerField.val(getRightAnswer());
    }

    function refreshValuesField(){
        valuesField.val(JSON.stringify(values));
    }
});