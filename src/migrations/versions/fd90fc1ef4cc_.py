"""empty message

Revision ID: fd90fc1ef4cc
Revises: 9d6b2ecc76d5
Create Date: 2017-06-10 21:32:20.165463

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'fd90fc1ef4cc'
down_revision = '9d6b2ecc76d5'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('material_group',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('material_id', sa.Integer(), nullable=False),
    sa.Column('classroom_id', sa.Integer(), nullable=False),
    sa.ForeignKeyConstraint(['classroom_id'], ['classroom.id'], ),
    sa.ForeignKeyConstraint(['material_id'], ['material.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('material_group')
    # ### end Alembic commands ###
